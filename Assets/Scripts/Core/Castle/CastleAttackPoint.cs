﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Castle
{
    public class CastleAttackPoint : MonoBehaviour
    {
        public static List<CastleAttackPoint> CastleAttackPoints = new List<CastleAttackPoint>();

        private void OnEnable()
        {
            if (!CastleAttackPoints.Contains(this))
                CastleAttackPoints.Add(this);
        }

        private void OnDisable()
        {
            if (CastleAttackPoints.Contains(this))
                CastleAttackPoints.Remove(this);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 0.25f);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 0.5f);
        }
    }
}