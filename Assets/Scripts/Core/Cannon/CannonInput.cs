﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Cannon
{
    public class CannonInput : MonoBehaviour
    {
        public static List<CannonInput> AllCannons = new List<CannonInput>();

        [SerializeField]
        private Transform _YPivotTransform = null;
        [SerializeField]
        private Transform _XPivotTransform = null;
        [SerializeField]
        private float _shootRate = 0.5f;
        [SerializeField]
        private float _shootRadius = 5f;
        [SerializeField]
        private float _damage = 50f;
        [SerializeField]
        private float _bulletSpeed = 10f;
        [SerializeField]
        private Transform _shootPoint = null;

        [SerializeField]
        private float _cannonAngleGain = 0.5f;

        private float _timer = 0f;

        private void OnEnable()
        {
            if (!AllCannons.Contains(this))
                AllCannons.Add(this);
        }

        private void OnDisable()
        {
            if (AllCannons.Contains(this))
                AllCannons.Remove(this);
        }

        public void SetTimer(float timer)
        {
            _timer = timer;
        }

        public float GetTimer()
        {
            return _timer;
        }

        private void Update()
        {
            for (int i = 0; i < Bullet.EnabledBullets.Count; i++)
            {
                Bullet.EnabledBullets[i].Check(Time.deltaTime);
            }

            var nearestTarget = Character.CharacterInput.GetNearestCharacter(_XPivotTransform.position, _shootRadius);
            if (nearestTarget != null)
            {
                var dist = Vector3.Distance(_XPivotTransform.position, nearestTarget.transform.position);
                var time = dist / _bulletSpeed;
                var targetPos = nearestTarget.transform.position + nearestTarget.GetVelocity() * time;

                var oldYAngle = _YPivotTransform.localEulerAngles;
                _YPivotTransform.LookAt(targetPos);
                oldYAngle.y = _YPivotTransform.localEulerAngles.y;
                _YPivotTransform.localEulerAngles = oldYAngle;

                var oldXAngle = _XPivotTransform.localEulerAngles;
                var inhTime = Time.deltaTime / time;
                _XPivotTransform.LookAt(Vector3.Lerp(_XPivotTransform.position, targetPos, inhTime) + Vector3.up * Mathf.Sin(Mathf.PI * inhTime) * _cannonAngleGain);
                oldXAngle.x = _XPivotTransform.localEulerAngles.x;
                _XPivotTransform.localEulerAngles = oldXAngle;

                _timer += Time.deltaTime;
                if (_timer > _shootRate)
                {
                    _timer = 0;
                    Bullet.Spawn(_shootPoint.position, time, targetPos, nearestTarget, _damage, _cannonAngleGain);
                }
            }
        }
    }
}