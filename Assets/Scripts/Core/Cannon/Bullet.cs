﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Cannon
{
    public class Bullet : MonoBehaviour
    {
        public static List<Bullet> DisabledBullets = new List<Bullet>();
        public static List<Bullet> EnabledBullets = new List<Bullet>();

        private Vector3 _startPos = Vector3.zero;
        private Vector3 _endPos = Vector3.zero;
        private float _flyTime = 0f;
        private float _damage = 0f;
        private float _angleGain = 0f;

        private Character.CharacterInput _characterInput = null;

        private float _timer = 0f;

        public static Bullet Spawn(Vector3 startPos, float flyTime, Vector3 endPos, Character.CharacterInput characterInput, float damage, float angleGain, string path = "Bullets/Bullet_0")
        {
            Bullet tempBullet = null;
            if (DisabledBullets.Count > 0)
            {
                tempBullet = DisabledBullets[0];
            }
            else
            {
                var bulletPref = Resources.Load<GameObject>(path);
                var tempBulletGO = Instantiate(bulletPref);
                tempBullet = tempBulletGO.GetComponent<Bullet>();
            }
            tempBullet.Init(startPos, flyTime, endPos, characterInput, damage, angleGain);
            return tempBullet;
        }

        public void SetDamage(float damage)
        {
            _damage = damage;
        }

        public float GetDamage()
        {
            return _damage;
        }

        public void SetAngleGain(float angleGain)
        {
            _angleGain = angleGain;
        }

        public float GetAngleGain()
        {
            return _angleGain;
        }

        public void SetFlyTime(float flyTime)
        {
            _flyTime = flyTime;
        }

        public float GetFlyTime()
        {
            return _flyTime;
        }

        public void SetStartPos(Vector3 startPos)
        {
            _startPos = startPos;
        }

        public Vector3 GetEndPos()
        {
            return _endPos;
        }

        public void SetEndPos(Vector3 endPos)
        {
            _endPos = endPos;
        }

        public Vector3 GetStartPos()
        {
            return _startPos;
        }

        public void SetCharacterInput(Character.CharacterInput characterInput)
        {
            _characterInput = characterInput;
        }

        public Character.CharacterInput GetCharacterInput()
        {
            return _characterInput;
        }

        public void SetTimer(float timer)
        {
            _timer = timer;
        }

        public float GetTimer()
        {
            return _timer;
        }

        private void OnEnable()
        {
            if (DisabledBullets.Contains(this))
                DisabledBullets.Remove(this);
            if (!EnabledBullets.Contains(this))
                EnabledBullets.Add(this);
        }

        private void OnDisable()
        {
            if (!DisabledBullets.Contains(this))
                DisabledBullets.Add(this);
            if (EnabledBullets.Contains(this))
                EnabledBullets.Remove(this);
        }

        public void OnDestroy()
        {
            if (DisabledBullets.Contains(this))
                DisabledBullets.Remove(this);
            if (EnabledBullets.Contains(this))
                EnabledBullets.Remove(this);
        }

        public void Init(Vector3 startPos, float flyTime, Vector3 endPos, Character.CharacterInput characterInput, float damage, float angleGain)
        {
            _timer = 0f;
            _startPos = startPos;
            _endPos = endPos;
            _flyTime = flyTime;
            _characterInput = characterInput;
            _damage = damage;
            _angleGain = angleGain;
            gameObject.SetActive(true);
        }

        public void Check(float deltaTime)
        {
            _timer += deltaTime;

            var tempInhTimer = _timer / _flyTime;
            transform.position = Vector3.Lerp(_startPos, _endPos, tempInhTimer) + Vector3.up * Mathf.Sin(Mathf.PI * tempInhTimer) * _angleGain;
            if (_timer >= _flyTime)
            {
                _characterInput.Damage(_damage);
                gameObject.SetActive(false);
            }
        }
    }
}

