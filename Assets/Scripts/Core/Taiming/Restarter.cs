﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Taiming
{
    public class Restarter : MonoBehaviour
    {
        //Invoke via button
        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }
}

