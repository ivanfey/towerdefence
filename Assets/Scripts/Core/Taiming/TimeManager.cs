﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Taiming
{
    public class TimeManager : MonoBehaviour
    {
        [System.Serializable]
        public struct CharacterTime
        {
            public Vector3 Pos;
            public Quaternion Rot;
            public float Health;
            public Transform TargetPoint;

            public int AnimNameHash;
            public float AnimNormalizeLenght;
        }

        [System.Serializable]
        public struct BulletTime
        {
            public Vector3 StartPos;
            public Vector3 EndPos;
            public float FlyTime;
            public float Damage;
            public float AngleGain;

            public Character.CharacterInput CharacterInput;

            public float Timer;
        }

        [System.Serializable]
        public struct CannonTime
        {
            public Cannon.CannonInput Cannon;
            public float Timer;
        }

        [System.Serializable]
        public class Frame
        {
            public float ActualTime = 0f;
            public List<CharacterTime> CharTimes = new List<CharacterTime>();
            public List<BulletTime> BulletTimes = new List<BulletTime>();
            public List<CannonTime> CannonTimes = new List<CannonTime>();
        }
        
        public static bool IsPause = false;

        [SerializeField]
        private List<Frame> _frames = new List<Frame>();
        [SerializeField]
        private Text _textTimer = null;
        [SerializeField]
        private Slider _slider = null;
        [SerializeField]
        private float _saveRate = 0.5f;

        private float _timer = 0f;

        private void Start()
        {
            SaveFrame();
        }

        private void Update()
        {
            if (IsPause)
                return;

            _textTimer.text = Mathf.FloorToInt(Time.timeSinceLevelLoad).ToString();

            _timer += Time.deltaTime;
            if(_timer >= _saveRate)
            {
                _timer = 0f;
                SaveFrame();
            }
        }

        public void SaveFrame()
        {
            if (IsPause)
                return;

            var tempFrame = new Frame();
            tempFrame.ActualTime = Time.timeSinceLevelLoad;
            for (int i = 0; i < Character.CharacterInput.AllEnabledCharacters.Count; i++)
            {
                var tempCharTime = new CharacterTime();
                tempCharTime.Pos = Character.CharacterInput.AllEnabledCharacters[i].transform.position;
                tempCharTime.Rot = Character.CharacterInput.AllEnabledCharacters[i].transform.rotation;
                tempCharTime.Health = Character.CharacterInput.AllEnabledCharacters[i].GetHealth();
                tempCharTime.TargetPoint = Character.CharacterInput.AllEnabledCharacters[i].GetTargetPoint();

                var tempAnim = Character.CharacterInput.AllEnabledCharacters[i].GetAnim();
                tempCharTime.AnimNameHash = tempAnim.GetCurrentAnimatorStateInfo(0).shortNameHash;
                tempCharTime.AnimNormalizeLenght = tempAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;

                tempFrame.CharTimes.Add(tempCharTime);
            }
            for (int i = 0; i < Cannon.Bullet.EnabledBullets.Count; i++)
            {
                var tempBulletTime = new BulletTime();
                tempBulletTime.StartPos = Cannon.Bullet.EnabledBullets[i].GetStartPos();
                tempBulletTime.EndPos = Cannon.Bullet.EnabledBullets[i].GetEndPos();
                tempBulletTime.FlyTime = Cannon.Bullet.EnabledBullets[i].GetFlyTime();
                tempBulletTime.Damage = Cannon.Bullet.EnabledBullets[i].GetDamage();
                tempBulletTime.AngleGain = Cannon.Bullet.EnabledBullets[i].GetAngleGain();
                tempBulletTime.CharacterInput = Cannon.Bullet.EnabledBullets[i].GetCharacterInput();
                tempBulletTime.Timer = Cannon.Bullet.EnabledBullets[i].GetTimer();

                tempFrame.BulletTimes.Add(tempBulletTime);
            }
            for (int i = 0; i < Cannon.CannonInput.AllCannons.Count; i++)
            {
                var tempCannon = new CannonTime();
                tempCannon.Cannon = Cannon.CannonInput.AllCannons[i];
                tempCannon.Timer = Cannon.CannonInput.AllCannons[i].GetTimer();

                tempFrame.CannonTimes.Add(tempCannon);
            }

            _frames.Add(tempFrame);

            _slider.maxValue = _frames.Count-1;
            _slider.value = _slider.maxValue;
        }

        public void LoadFrame(float frame)
        {
            if (!IsPause)
                return;
            
            var tempFrame = Mathf.RoundToInt(frame);
            _textTimer.text = Mathf.FloorToInt(_frames[tempFrame].ActualTime).ToString();

            var enabledCharacters = new Character.CharacterInput[Character.CharacterInput.AllEnabledCharacters.Count];
            for (int i = 0; i < Character.CharacterInput.AllEnabledCharacters.Count; i++)
            {
                enabledCharacters[i] = Character.CharacterInput.AllEnabledCharacters[i];
            }
            for (int i = 0; i < enabledCharacters.Length; i++)
            {
                enabledCharacters[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < _frames[tempFrame].CharTimes.Count; i++)
            {
                var tempCharTime = _frames[tempFrame].CharTimes[i];
                var tempCharacterInput = Character.CharacterSpawner.Spawn(tempCharTime.Pos);
                tempCharacterInput.transform.rotation = tempCharTime.Rot;
                tempCharacterInput.SetHealth(tempCharTime.Health);
                tempCharacterInput.SetTargetPoint(tempCharTime.TargetPoint);

                var tempAnim = tempCharacterInput.GetAnim();
                tempAnim.Play(tempCharTime.AnimNameHash, 0, tempCharTime.AnimNormalizeLenght);
            }

            var enabledBullets = new Cannon.Bullet[Cannon.Bullet.EnabledBullets.Count];
            for (int i = 0; i < Cannon.Bullet.EnabledBullets.Count; i++)
            {
                enabledBullets[i] = Cannon.Bullet.EnabledBullets[i];
            }
            for (int i = 0; i < enabledBullets.Length; i++)
            {
                enabledBullets[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < _frames[tempFrame].BulletTimes.Count; i++)
            {
                var tempBulletTime = _frames[tempFrame].BulletTimes[i];
                var tempBullet = Cannon.Bullet.Spawn(tempBulletTime.StartPos, tempBulletTime.FlyTime, tempBulletTime.EndPos, tempBulletTime.CharacterInput, tempBulletTime.Damage, tempBulletTime.AngleGain);
                tempBullet.SetTimer(tempBulletTime.Timer);
            }

            for (int i = 0; i < _frames[tempFrame].CannonTimes.Count; i++)
            {
                _frames[tempFrame].CannonTimes[i].Cannon.SetTimer(_frames[tempFrame].CannonTimes[i].Timer);
            }
        }

        public void Pause()
        {
            SaveFrame();
            IsPause = true;
            Time.timeScale = 0f;

        }

        public void Play()
        {
            _frames.RemoveRange(Mathf.RoundToInt(_slider.value), _frames.Count - Mathf.RoundToInt(_slider.value));
            IsPause = false;
            Time.timeScale = 1f;
        }
    }
}