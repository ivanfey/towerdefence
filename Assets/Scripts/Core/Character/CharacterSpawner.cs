﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Character
{
    public class CharacterSpawner : MonoBehaviour
    {
        [SerializeField]
        private string _groundTag = "Ground";

        public static CharacterInput Spawn(Vector3 position, string path = "Characters/Goblin")
        {
            GameObject tempCharacter = null;
            CharacterInput tempCharInput = null;
            if (CharacterInput.AllDisabledCharacters.Count > 0)
            {
                tempCharacter = CharacterInput.AllDisabledCharacters[0].gameObject;
                tempCharInput = CharacterInput.AllDisabledCharacters[0];
            }
            else
            {
                var characterPref = Resources.Load<GameObject>(path);
                tempCharacter = Instantiate(characterPref);
                tempCharInput = tempCharacter.GetComponent<CharacterInput>();
            }
            tempCharacter.transform.position = position;
            tempCharacter.SetActive(true);

            return tempCharInput;
        }

        private void Update()
        {
            if (Taiming.TimeManager.IsPause)
                return;
            if (!Input.GetMouseButtonUp(0))
                return;

            var ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);
            var hit = new RaycastHit();
            if (!Physics.Raycast(ray, out hit, float.MaxValue, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                return;
            if (!hit.transform.CompareTag(_groundTag))
                return;

            Spawn(hit.point);
        }
    }
}

