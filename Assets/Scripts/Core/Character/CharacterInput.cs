﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Core.Character
{
    public class CharacterInput : MonoBehaviour
    {
        public static List<CharacterInput> AllDisabledCharacters = new List<CharacterInput>();
        public static List<CharacterInput> AllEnabledCharacters = new List<CharacterInput>();

        [SerializeField]
        private string _verticalInputParamName = "Vertical";
        [SerializeField]
        private string _attackInputParamName = "Attack";
        [SerializeField]
        private float _attackDistance = 1;
        [SerializeField]
        private float _rotateToTargetSpeed = 15f;
        [SerializeField]
        private float _rotateToTargetDistance = 3f;
        [SerializeField]
        private float _startHealth = 100f;

        private float _health = 0f;

        private NavMeshAgent _navAgent = null;
        private Animator _anim = null;

        private Transform _targetPoint = null;

        private void Awake()
        {
            _navAgent = GetComponent<NavMeshAgent>();
            _anim = GetComponent<Animator>();
        }

        public void SetTargetPoint(Transform targetPoint)
        {
            _targetPoint = targetPoint;
            if(targetPoint != null)
                _navAgent.SetDestination(_targetPoint.position);
        }

        public Transform GetTargetPoint()
        {
            return _targetPoint;
        }

        public void SetHealth(float health)
        {
            _health = health;
        }

        public float GetHealth()
        {
            return _health;
        }

        public static CharacterInput GetNearestCharacter(Vector3 origin, float maxRadius)
        {
            var minDist = float.MaxValue;
            CharacterInput nearestCharInput = null;
            for (int i = 0; i < AllEnabledCharacters.Count; i++)
            {
                var dist = Vector3.SqrMagnitude(AllEnabledCharacters[i].transform.position - origin);
                if (dist >= maxRadius)
                    continue;
                if (dist >= minDist)
                    continue;

                minDist = dist;
                nearestCharInput = AllEnabledCharacters[i];
            }
            return nearestCharInput;
        }

        private void OnEnable()
        {
            if (AllDisabledCharacters.Contains(this))
                AllDisabledCharacters.Remove(this);
            if (!AllEnabledCharacters.Contains(this))
                AllEnabledCharacters.Add(this);

            _health = _startHealth;
            var navHit = new NavMeshHit();
            var isNearestGround = NavMesh.SamplePosition(transform.position, out navHit, float.MaxValue, -1);
            if (isNearestGround)
                transform.position = navHit.position;

            _targetPoint = FindTargetPoint();

            if (_targetPoint == null)
                enabled = false;
            else
                _navAgent.SetDestination(_targetPoint.position);
        }

        private void OnDisable()
        {
            if (!AllDisabledCharacters.Contains(this))
                AllDisabledCharacters.Add(this);
            if (AllEnabledCharacters.Contains(this))
                AllEnabledCharacters.Remove(this);
        }

        private void OnDestroy()
        {
            if (AllEnabledCharacters.Contains(this))
                AllEnabledCharacters.Remove(this);
            if (AllDisabledCharacters.Contains(this))
                AllDisabledCharacters.Remove(this);
        }

        private Transform FindTargetPoint()
        {
            var allPoints = Castle.CastleAttackPoint.CastleAttackPoints;
            return allPoints[Random.Range(0, allPoints.Count)].transform;
        }

        private void Update()
        {
            var squareMagnitude = Vector3.SqrMagnitude(_targetPoint.position - transform.position);
            _anim.SetBool(_attackInputParamName, squareMagnitude <= _attackDistance * _attackDistance);
            _navAgent.isStopped = squareMagnitude <= _attackDistance * _attackDistance;

            var velocity = transform.InverseTransformDirection(_navAgent.velocity);
            _anim.SetFloat(_verticalInputParamName, velocity.z);

            if (squareMagnitude <= _rotateToTargetDistance * _rotateToTargetDistance)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, _targetPoint.rotation, Time.deltaTime * _rotateToTargetSpeed);
            }
        }

        public Vector3 GetVelocity()
        {
            return _navAgent.velocity;
        }

        private void GetDamage()
        {
            //TODO damage here...
            gameObject.SetActive(false);
        }

        public void Damage(float damage)
        {
            _health -= damage;
            if (_health <= 0)
                gameObject.SetActive(false);
        }

        public Animator GetAnim()
        {
            return _anim;
        }
    }
}

